<?php

namespace Elabs\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class QueryController extends Controller
{
    /**
     * Listar los resultados en funcion 
     * de la peticion recibida del formulario.
     *
     */
    public function queryList(Request $request)
    {
        $query =  $request->input('busqueda');

        $resultset = DB::table('resultados_deportivos')
            ->select('fecha','local','temporada', 'visitante', 'gol_local', 'gol_visitante', 'estadio')
            ->where( 'local', 'like', '%'.$query.'%' )
            ->orwhere('visitante','like','%'. $query.'%')
            ->orderby('temporada')
            ->get();
        
        return view('resultados', compact('resultset'));
    }

    public function fichaEquipo($name)
    {
        $jornadas = DB::table('resultados_deportivos')
            ->select('jornada')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->orwhere('visitante','like','%'. $name.'%')
            ->where(function ($query) {
                $query->where('temporada', '1928-29');
            })
            ->groupBy('jornada')
            ->get();

        $partido_jugados = count($jornadas);
        $partido_ganados_local = DB::table('resultados_deportivos')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->where('gol_local', '>','gol_visitante')
            ->where('temporada', 'like', '1928-29')
            ->count();   
   
        $partido_ganados_visitante = DB::table('resultados_deportivos')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->where('gol_local', '<','gol_visitante')
            ->where('temporada', 'like', '1928-29')
            ->count(); 
        $partido_ganados = $partido_ganados_local + $partido_ganados_visitante; 

        $partido_empatados_local = DB::table('resultados_deportivos')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->whereColumn('gol_local', 'gol_visitante')
            ->where('temporada', 'like', '1928-29')
            ->count();   
   
        $partido_empatados_visitante = DB::table('resultados_deportivos')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->whereColumn('gol_local', 'gol_visitante')
            ->where('temporada', 'like', '1928-29')
            ->count(); 
        $partido_empatados = $partido_empatados_local + $partido_empatados_visitante; 
        $goles_contra_local = DB::table('resultados_deportivos')
            ->select('gol_local')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->where('gol_local', '>', 'gol_visitante')
            ->where('temporada', 'like', '1928-29')
            ->sum('gol_local');
        $goles_contra_visitante = DB::table('resultados_deportivos')
            ->select('gol_visitante')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->where('gol_local', '<', 'gol_visitante')
            ->where('temporada', 'like', '1928-29')
            ->sum('gol_visitante');
        $goles_contra = $goles_contra_local + $goles_contra_visitante;
        $goles_favor_local = DB::table('resultados_deportivos')
            ->select('gol_local')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->where('gol_local', '>', 'gol_visitante')
            ->where('temporada', 'like', '1928-29')
            ->sum('gol_local');
        $goles_favor_visitante = DB::table('resultados_deportivos')
            ->select('gol_visitante')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->where('gol_local', '<', 'gol_visitante')
            ->where('temporada', 'like', '1928-29')
            ->sum('gol_visitante');
        $goles_favor = $goles_favor_local + $goles_favor_visitante;

            $estadisticas_2829 = [
                "partidos_jugados" => $partido_jugados,
                "partidos_ganados" => $partido_ganados,
                "partidos_empatados" => $partido_empatados,
                "goles_contra" => $goles_contra,
                "goles_favor" => $goles_favor,
            ];

        $jornadas = DB::table('resultados_deportivos')
            ->select('jornada')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->orwhere('visitante','like','%'. $name.'%')
            ->where(function ($query) {
                $query->where('temporada', '1929-30');
            })
            ->groupBy('jornada')
            ->get();

        $partido_jugados = count($jornadas);

        $partido_ganados_local = DB::table('resultados_deportivos')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->where('gol_local', '>','gol_visitante')
            ->where('temporada', 'like', '1929-30')
            ->count();   
   
        $partido_ganados_visitante = DB::table('resultados_deportivos')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->where('gol_local', '<','gol_visitante')
            ->where('temporada', 'like', '1929-30')
            ->count(); 
        $partido_ganados = $partido_ganados_local + $partido_ganados_visitante; 

        $partido_empatados_local = DB::table('resultados_deportivos')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->whereColumn('gol_local', 'gol_visitante')
            ->where('temporada', 'like', '1929-30')
            ->count();   
   
        $partido_empatados_visitante = DB::table('resultados_deportivos')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->whereColumn('gol_local', 'gol_visitante')
            ->where('temporada', 'like', '1929-30')
            ->count(); 
        $partido_empatados = $partido_empatados_local + $partido_empatados_visitante; 
        $goles_contra_local = DB::table('resultados_deportivos')
            ->select('gol_local')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->where('gol_local', '>', 'gol_visitante')
            ->where('temporada', 'like', '1929-30')
            ->sum('gol_local');
        $goles_contra_visitante = DB::table('resultados_deportivos')
            ->select('gol_visitante')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->where('gol_local', '<', 'gol_visitante')
            ->where('temporada', 'like', '1929-30')
            ->sum('gol_visitante');
        $goles_contra = $goles_contra_local + $goles_contra_visitante;
        $goles_favor_local = DB::table('resultados_deportivos')
            ->select('gol_local')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->where('gol_local', '>', 'gol_visitante')
            ->where('temporada', 'like', '1929-30')
            ->sum('gol_local');
        $goles_favor_visitante = DB::table('resultados_deportivos')
            ->select('gol_visitante')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->where('gol_local', '<', 'gol_visitante')
            ->where('temporada', 'like', '1929-30')
            ->sum('gol_visitante');
        $goles_favor = $goles_favor_local + $goles_favor_visitante;

            $estadisticas_2930 = [
                "partidos_jugados" => $partido_jugados,
                "partidos_ganados" => $partido_ganados,
                "partidos_empatados" => $partido_empatados,
                "goles_contra" => $goles_contra,
                "goles_favor" => $goles_favor,
            ];
        $jornadas = DB::table('resultados_deportivos')
            ->select('jornada')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->orwhere('visitante','like','%'. $name.'%')
            ->where(function ($query) {
                $query->where('temporada', '1930-31');
            })
            ->groupBy('jornada')
            ->get();

        $partido_jugados = count($jornadas);            

        $partido_ganados_local = DB::table('resultados_deportivos')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->where('gol_local', '>','gol_visitante')
            ->where('temporada', 'like', '1930-31')
            ->count();   
   
        $partido_ganados_visitante = DB::table('resultados_deportivos')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->where('gol_local', '<','gol_visitante')
            ->where('temporada', 'like', '1930-31')
            ->count(); 
        $partido_ganados = $partido_ganados_local + $partido_ganados_visitante; 

        $partido_empatados_local = DB::table('resultados_deportivos')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->whereColumn('gol_local', 'gol_visitante')
            ->where('temporada', 'like', '1930-31')
            ->count();   
   
        $partido_empatados_visitante = DB::table('resultados_deportivos')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->whereColumn('gol_local', 'gol_visitante')
            ->where('temporada', 'like', '1930-31')
            ->count(); 
        $partido_empatados = $partido_empatados_local + $partido_empatados_visitante; 
        $goles_contra_local = DB::table('resultados_deportivos')
            ->select('gol_local')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->where('gol_local', '>', 'gol_visitante')
            ->where('temporada', 'like', '1930-31')
            ->sum('gol_local');
        $goles_contra_visitante = DB::table('resultados_deportivos')
            ->select('gol_visitante')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->where('gol_local', '<', 'gol_visitante')
            ->where('temporada', 'like', '1930-31')
            ->sum('gol_visitante');
        $goles_contra = $goles_contra_local + $goles_contra_visitante;
        $goles_favor_local = DB::table('resultados_deportivos')
            ->select('gol_local')
            ->where( 'local', 'like', '%'.$name.'%' )
            ->where('gol_local', '>', 'gol_visitante')
            ->where('temporada', 'like', '1930-31')
            ->sum('gol_local');
        $goles_favor_visitante = DB::table('resultados_deportivos')
            ->select('gol_visitante')
            ->where( 'visitante', 'like', '%'.$name.'%' )
            ->where('gol_local', '<', 'gol_visitante')
            ->where('temporada', 'like', '1930-31')
            ->sum('gol_visitante');
        $goles_favor = $goles_favor_local + $goles_favor_visitante;


        $estadisticas_3031 = [
                "partidos_jugados" => $partido_jugados,
                "partidos_ganados" => $partido_ganados,
                "partidos_empatados" => $partido_empatados,
                "goles_contra" => $goles_contra,
                "goles_favor" => $goles_favor,
            ];

                        
            return view('ficha', compact('estadisticas_2829', 'estadisticas_2930', 'estadisticas_3031', 'name'));
            

    }
}
