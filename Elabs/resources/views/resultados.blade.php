@extends('templates.app')

@section('title','Resultados Busqueda')

@section('content')
<div class="row">
	<div class="col-sm">
			<form action="/resultados" class="form-group" method="POST">
		@csrf
		<div class="form-group">
			<input type="text" name="busqueda" class="form-control" placeholder="Busqueda">
			
		</div>
		<button type="submit" class="btn btn-primary">Buscar</button>
	</form>
	</div>
</div>
	<div class="row">
		<ul class="list-group">
			@foreach($resultset as $result)
				<li class="list-group">
					<div class="col-sm-12 col-md-12">
						[{{$result->temporada}}] <a href="ficha/{{$result->local}}"> {{$result->local}}</a> - <a href="ficha/{{$result->visitante}}">{{$result->visitante}} </a> {{$result->gol_local}} - {{$result->gol_visitante}}    ({{$result->fecha}} - {{$result->estadio}})
					</div>
				</li>
			@endforeach
		</ul>
	</div>
@endsection