@extends('templates.app')

@section('title','Ficha del equipo')

@section('content')
	<div class="row">
		<div class="text-center col-md-12 col-sm-12">
			<h1>{{$name}}</h1>			
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<h2>Estadísticas Temporada 28-29</h2>
			<p>Partidos Jugados: {{$estadisticas_2829["partidos_jugados"]}}</p>
			<p>Partidos Ganados: {{$estadisticas_2829["partidos_ganados"]}}</p>
			<p>Partidos Empatados: {{$estadisticas_2829["partidos_empatados"]}}</p>
			<p>Goles a favor: {{$estadisticas_2829["goles_favor"]}}</p>
			<p>Goles en contra: {{$estadisticas_2829["goles_contra"]}}</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<h2>Estadísticas Temporada 29-30</h2>
			<p>Partidos Jugados: {{$estadisticas_2930["partidos_jugados"]}}</p>
			<p>Partidos Ganados: {{$estadisticas_2930["partidos_ganados"]}}</p>
			<p>Partidos Empatados: {{$estadisticas_2930["partidos_empatados"]}}</p>
			<p>Goles a favor: {{$estadisticas_2930["goles_favor"]}}</p>
			<p>Goles en contra: {{$estadisticas_2930["goles_contra"]}}</p>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12 col-sm-12">
			<h2>Estadísticas Temporada 30-31</h2>
			<p>Partidos Jugados: {{$estadisticas_3031["partidos_jugados"]}}</p>
			<p>Partidos Ganados: {{$estadisticas_3031["partidos_ganados"]}}</p>
			<p>Partidos Empatados: {{$estadisticas_3031["partidos_empatados"]}}</p>
			<p>Goles a favor: {{$estadisticas_3031["goles_favor"]}}</p>
			<p>Goles en contra: {{$estadisticas_3031["goles_contra"]}}</p>
		</div>
	</div>	

@endsection