## INSTALACIÓN DEL PROYECTO Y CONFIGURACIÓN

Para realizar la instalación copiaremos la carpeta "Elabs" del proyecto a nuestro servidor web, y crearemos una base de datos mysql con el archivo "resultados_deportivos.sql" que tenemos en la raiz del proyecto.
La configuración del servidor para la base de datos deberia corresponderse con los siguientes datos
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=elabs
	DB_USERNAME=root
	DB_PASSWORD=
Podriamos configurar estos datos modificando el archivo "Elabs/.env"

**FUNCIONALIDADES**

Esta webapp solo mostrara el listado de resultados, además de las fichas de detalle de cada equipo, cuando se busque un equipo,
. En el caso que se busquen dos equipos a la vez no mostrara resultados, esta seria una funcionalidad para implementar en nuevas versiones.



**Realizado por Jordi Nogueras**
